package com.github.ashabhasa.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;

public class ListUtils {
    public static <A, B> B foldLeft(Collection<A> elements, B initialValue, BiFunction<B, A, B> folder) {

        Iterator<A> iterator = elements.iterator();
        B acc = initialValue;

        while (iterator.hasNext())
            acc = folder.apply(acc, iterator.next());

        return acc;
    }

    public static <A, B> List<B> map(Collection<A> elements, Function<A, B> mapper) {
        List<B> res = new ArrayList<>();
        for (A el : elements)
            res.add(mapper.apply(el));

        return res;
    }

    public static <A> List<A> flatten(List<List<A>> elements) {

        List<A> res = new ArrayList<>();
        for (List<A> el : elements)
            res.addAll(el);

        return res;
    }

    public static <A extends B, B> B reduce(Collection<A> elements, BiFunction<B, A, B> reducer) {

        Iterator<A> iterator = elements.iterator();
        B acc = iterator.next();

        while (iterator.hasNext())
            acc = reducer.apply(acc, iterator.next());

        return acc;
    }
}
