## DB Benchmarks ##

You have to implement simple java application which executes benchmarks on a JDBC compliant database.
The application MUST evaluate the min/max/avg time of INSERT statements.
The application MUST evaluate the min/max/avg time of SELECT statements (using the PK of the COLUMN).
The application MUST issue DML requests using the PreparedStatement API and issuing “commits” every X statements.
The application will be run on a PostGRE or MS SQLServer database.
The script to create the table MUST be included in the source code.

#Solution#
You'll find in the resources the file world.sql, it contains the script for the creation and population of postgresql database.
The Application class contains the main method that launches the Application

The application will perform 4000 selects and 100 dml statements performing a commit every 4 statements and  then print the max min and avg time.
The results of the query and dml statements have been ignored.