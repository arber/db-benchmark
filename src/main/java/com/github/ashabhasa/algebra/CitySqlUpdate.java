package com.github.ashabhasa.algebra;

import com.github.ashabhasa.models.City;
import com.github.ashabhasa.utils.Try;

import java.sql.*;

public class CitySqlUpdate implements SqlUpdate<City> {

    private final Connection conn;

    private static final String insertCity = "INSERT INTO city(name, countrycode, district, population) VALUES(?,?,?,?)";

    public CitySqlUpdate(Connection conn) {
        this.conn = conn;
    }

    private Try<Integer> executeUpdate(String dml, City city) {
        try (PreparedStatement stmt = conn.prepareStatement(dml, Statement.RETURN_GENERATED_KEYS)) {
            stmt.setString(1, city.getName());
            stmt.setString(2, city.getCountryCode());
            stmt.setString(3, city.getDistrict());
            stmt.setInt(4, city.getPopulation());

            int id = stmt.executeUpdate();

            if (id > 0) {
                try (ResultSet rs = stmt.getGeneratedKeys()) {
                    if (rs.next()) return Try.success(rs.getInt(1));
                } catch (SQLException e) {
                    Try.failure(e);
                }
            }
        } catch (SQLException ex) {
            return Try.failure(ex);
        }

        return Try.success(0);
    }

    @Override
    public Try<Integer> insert(City city) {
        return executeUpdate(insertCity, city);
    }
}
