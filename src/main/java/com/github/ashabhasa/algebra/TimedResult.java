package com.github.ashabhasa.algebra;

import com.github.ashabhasa.utils.Duration;
import com.github.ashabhasa.utils.Pair;

public interface TimedResult<A> {
    Pair<A, Duration> getTimedResult();
}
