package com.github.ashabhasa.db;

import com.github.ashabhasa.models.City;
import com.github.ashabhasa.utils.Option;

public interface CityRepository {
    Option<City> findCity(int id);
}
