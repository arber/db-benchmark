package com.github.ashabhasa.db;

import com.github.ashabhasa.models.City;
import com.github.ashabhasa.utils.Option;
import com.github.ashabhasa.utils.Try;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class CityRepositoryImpl implements CityRepository {
    private final Connection conn;

    private static final String selectCityById = "SELECT * FROM city WHERE id = ?";

    private final Function<ResultSet, Try<City>> toCityModel = resultSet -> {
        try {
            String name = resultSet.getString("name");
            String countryCode = resultSet.getString("countrycode");
            String district = resultSet.getString("district");
            int population = resultSet.getInt("population");
            return Try.success(new City(name, countryCode, district, population));
        } catch (SQLException e) {
            return Try.failure(e);
        }
    };

    public CityRepositoryImpl(Connection conn) {
        this.conn = conn;
    }


    @Override
    public Option<City> findCity(int id) {
        List<City> cityResult = executeQuery(selectCityById, id, toCityModel).get();
        Try<City> map = Try.fromSupplier("try get first city", () -> cityResult.get(0));
        return (map.isSuccess()) ? Option.some(map.get()) : Option.none();
    }

    private <O> Try<List<O>> executeQuery(String sqlQuery, int pk, Function<ResultSet, Try<O>> toModel) {
        try (PreparedStatement stmt = conn.prepareStatement(sqlQuery)) {
            stmt.setInt(1, pk);
            ResultSet rs = stmt.executeQuery();
            List<O> res = new ArrayList<>();
            while (rs.next()) {
                Try<O> apply = toModel.apply(rs);
                if (apply.isSuccess())
                    res.add(apply.get());
                else return Try.failure(apply.getFailure());
            }
            return Try.success(res);

        } catch (SQLException ex) {
            return Try.failure(ex);
        }

    }
}
