package com.github.ashabhasa;

import com.github.ashabhasa.algebra.*;
import com.github.ashabhasa.models.City;
import com.github.ashabhasa.utils.ListUtils;
import com.github.ashabhasa.utils.Option;
import com.github.ashabhasa.utils.Try;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class AlgebraJdbcTest {

    private static String jdbcUrl = "jdbc:postgresql://localhost/world";
    private final String user = "postgres";
    private final String password = "postgres";


    @Test
    void secondTest() {
        SqlQuery<Option<City>> cityQuery = new CityQuery(createConnection().get());
        TimedQuery<Option<City>> timed = new TimedQuery<>(cityQuery);

        TimedResult<Option<City>> timedResult = timed.getById(1465);
        Option<City> cityOption = timedResult.getTimedResult().getLeft();
        assertTrue(cityOption.isDefined());
        assertEquals("Lombardia", timedResult.getTimedResult().getLeft().getValue().getDistrict());
        String msg = String.format("Elapsed time: %s ms", timedResult.getTimedResult().getRight().getMillis());
        System.out.println(msg);
    }

    @Test
    void noRecordFound() {
        SqlQuery<Option<City>> cityQuery = new CityQuery(createConnection().get());
        TimedQuery<Option<City>> timed = new TimedQuery<>(cityQuery);

        TimedResult<Option<City>> city = timed.getById(-1);
        Option<City> cityOption = city.getTimedResult().getLeft();
        assertTrue(cityOption.isEmpty());
        String msg = String.format("Elapsed time: %s ms", city.getTimedResult().getRight().getMillis());
        System.out.println(msg);
    }

    @Test
    void testAlternativeImplementationOfTimingQueries() {
        List<Integer> ids = streamRange(1, 5000);
        SqlQuery<Option<City>> cityQuery = new CityQuery(createConnection().get());

        List<Supplier<TimedResult<Option<City>>>> queries = ListUtils.map(ids, createTimedQueries(cityQuery));

        List<Long> times = ListUtils.map(queries, timedResultSupplier -> timedResultSupplier.get().getTimedResult().getRight().getMicros());

        Long maxTime = ListUtils.reduce(times, AlgebraJdbcTest::max);
        Long minTime = ListUtils.reduce(times, AlgebraJdbcTest::min);

        Long avg = ListUtils.foldLeft(times, 0L, (acc, l) -> acc + l) / times.size();

        String msg = String.format("Max time:%s µs. Min time: %s µs. Avg time: %s µs. ", maxTime, minTime, avg);
        System.out.println(msg);
    }

    private Function<Integer, Supplier<TimedResult<Option<City>>>> createTimedQueries(SqlQuery<Option<City>> cityQuery) {
        return id -> () -> TimeResult.time(() -> cityQuery.getById(id));
    }

    private static long min(long a, long b) {
        return a <= b ? a : b;
    }

    private static long max(long a, long b) {
        return a >= b ? a : b;
    }

    private List<Integer> streamRange(int from, int limit) {
        return IntStream.range(from, from + limit)
                .boxed()
                .collect(toList());
    }

    private Try<Connection> createConnection() {
        try {
            return Try.success(DriverManager.getConnection(jdbcUrl, user, password));
        } catch (SQLException e) {
            return Try.failure(e);
        }
    }

}
