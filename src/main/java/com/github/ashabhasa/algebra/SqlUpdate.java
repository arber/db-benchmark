package com.github.ashabhasa.algebra;

import com.github.ashabhasa.utils.Try;

public interface SqlUpdate<A> {
    Try<Integer> insert(A a);
}

