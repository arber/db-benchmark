package com.github.ashabhasa.utils;

import com.github.ashabhasa.models.City;

import java.util.Random;

public class CityGenerator {

    private final Random random;

    public CityGenerator(long seed) {
        random = new Random(seed);
    }

    public City create() {
        return new City("name_" + random.nextInt(10000), "ITA", "BO", 1234567);
    }
}
