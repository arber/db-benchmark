package com.github.ashabhasa.algebra;

public interface SqlQuery<A> {
    A getById(int id);
}

