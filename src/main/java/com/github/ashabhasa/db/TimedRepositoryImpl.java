package com.github.ashabhasa.db;

import com.github.ashabhasa.algebra.SqlQuery;
import com.github.ashabhasa.utils.Duration;
import com.github.ashabhasa.utils.Pair;


public class TimedRepositoryImpl<A> {

    private final SqlQuery<A> docoratee;

    public TimedRepositoryImpl(SqlQuery<A> docoratee) {
        this.docoratee = docoratee;
    }

    public Pair<A,Duration> getById(int id) {
        long startTime = System.nanoTime();
        A optCity = this.docoratee.getById(id);
        long endTime = System.nanoTime();
        return Pair.of(optCity, new Duration(startTime, endTime));
    }



}
