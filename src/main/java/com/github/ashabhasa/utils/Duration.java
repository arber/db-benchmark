package com.github.ashabhasa.utils;

public class Duration {
    private final long start;
    private final long end;

    public Duration(long start, long end) {

        this.start = start;
        this.end = end;
    }

    public long getStart() {
        return start;
    }

    public long getEnd() {
        return end;
    }

    public long getMillis(){
        return (getEnd() - getStart()) / 1000000;
    }

    public long getMicros(){
        return (getEnd() - getStart())/1000;
    }

    public long getNanos(){
        return (getEnd() - getStart());
    }
}
