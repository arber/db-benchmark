package com.github.ashabhasa.utils;

import java.util.NoSuchElementException;
import java.util.Objects;

public class Option<T> {
    private final T value;
    private final boolean empty;
    private static final Option<?> EMPTY = new Option((Object) null, true);

    private Option(T value, boolean empty) {
        this.value = value;
        this.empty = empty;
    }

    public static <T> Option<T> some(T v) {
        return new Option(v, false);
    }

    public static <T> Option<T> none() {
        return (Option<T>) EMPTY;
    }

    public T getValue() {
        if (this.isEmpty()) {
            throw new NoSuchElementException();
        } else {
            return this.value;
        }
    }

    public boolean isEmpty() {
        return this.empty;
    }

    public boolean isDefined() {
        return !this.isEmpty();
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        } else if (o != null && this.getClass() == o.getClass()) {
            Option<?> option = (Option) o;
            return this.empty == option.empty && Objects.equals(this.value, option.value);
        } else {
            return false;
        }
    }

    public int hashCode() {
        return Objects.hash(new Object[]{this.value, this.empty});
    }

    public String toString() {
        return this.isEmpty() ? "None()" : "Some(" + this.value.toString() + ")";
    }
}
