package com.github.ashabhasa.algebra;

import com.github.ashabhasa.utils.Duration;
import com.github.ashabhasa.utils.Pair;

import java.util.function.Supplier;

public class TimeResult {

    public static <O> TimedResult<O> time(Supplier<O> f) {
        final long startTime = System.nanoTime();
        final O res = f.get();
        final long endTime = System.nanoTime();
        return () -> Pair.of(res, new Duration(startTime, endTime));
    }


    public static <O> Pair<O, Duration> time2(Supplier<O> f) {
        final long startTime = System.nanoTime();
        final O res = f.get();
        final long endTime = System.nanoTime();
        return Pair.of(res, new Duration(startTime, endTime));
    }
}
