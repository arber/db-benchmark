package com.github.ashabhasa.algebra;

import com.github.ashabhasa.utils.Duration;
import com.github.ashabhasa.utils.Pair;

public class TimedQuery<A> implements SqlQuery<TimedResult<A>> {

    private final SqlQuery<A> sqlQuery;

    public TimedQuery(SqlQuery<A> sqlQuery) {
        this.sqlQuery = sqlQuery;
    }

    @Override
    public TimedResult<A> getById(int id) {
        final long startTime = System.nanoTime();
        final A a = sqlQuery.getById(id);
        final long endTime = System.nanoTime();
        return () -> Pair.of(a, new Duration(startTime, endTime));
    }
}
