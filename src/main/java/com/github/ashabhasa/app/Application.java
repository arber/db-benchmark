package com.github.ashabhasa.app;

import com.github.ashabhasa.algebra.*;
import com.github.ashabhasa.models.City;
import com.github.ashabhasa.utils.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;

public class Application {
    private static final String jdbcUrl = "jdbc:postgresql://localhost/world";
    private static final String user = "postgres";
    private static final String password = "postgres";

    public static void main(String[] args) {
        Connection conn = createConnection().get();
        trySetConnectionAutoCommit(conn, false);

        SqlQuery<Option<City>> cityQuery = new CityQuery(conn);

        List<Supplier<Pair<Option<City>, Duration>>> queries = ListUtils.map(range(1, 4000), createTimedQueries(cityQuery));

        printTimes("Sql queries", ListUtils.map(queries, timedResultSupplier -> timedResultSupplier.get().getRight().getMicros()));

        CitySqlUpdate citySqlUpdate = new CitySqlUpdate(conn);

        final CityGenerator cityGenerator = new CityGenerator(System.currentTimeMillis());

        List<City> cities = ListUtils.map(range(1, 100), integer -> cityGenerator.create());

        //Partiziono le richieste per per fare commit ogni x richieste.
        List<List<City>> partition = org.apache.commons.collections4.ListUtils.partition(cities, 4);

        //preparo lo statement da eseguire per ogni partizione
        List<List<Supplier<Pair<Try<Integer>, Duration>>>> insertResults = ListUtils.map(partition, prepareExecution(citySqlUpdate));

        //racologo solo il tmpo dello stametement ignorando il suo risultato
        final List<List<Long>> timeResults = ListUtils.map(insertResults, executeQueries(conn));
        printTimes("Insert queries", ListUtils.flatten(timeResults));

    }

    private static Function<List<Supplier<Pair<Try<Integer>, Duration>>>, List<Long>> executeQueries(Connection conn) {
        return suppliers -> {
            final List<Long> map = ListUtils.map(suppliers, pairSupplier -> pairSupplier.get().getRight().getMicros());
            // dopo ogni partizione faccio il commit
            tryCommit(conn);
            return map;
        };
    }

    private static Function<List<City>, List<Supplier<Pair<Try<Integer>, Duration>>>> prepareExecution(CitySqlUpdate citySqlUpdate) {
        //preparo i statement da eseguire
        return cities -> ListUtils.map(cities, city -> () -> TimeResult.time2(() -> citySqlUpdate.insert(city)));
    }


    private static void printTimes(String message, List<Long> times) {

        Long maxTime = ListUtils.reduce(times, Application::max);
        Long minTime = ListUtils.reduce(times, Application::min);
        Long avg = ListUtils.foldLeft(times, 0L, (acc, l) -> acc + l) / times.size();

        String msg = String.format("%s: Max time:%s µs. Min time: %s µs. Avg time: %s µs. ", message, maxTime, minTime, avg);
        System.out.println(msg);
    }

    private static Function<Integer, Supplier<Pair<Option<City>, Duration>>> createTimedQueries(SqlQuery<Option<City>> cityQuery) {
        return id -> () -> TimeResult.time2(() -> cityQuery.getById(id));
    }

    private static long min(long a, long b) {
        return a <= b ? a : b;
    }

    private static long max(long a, long b) {
        return a >= b ? a : b;
    }

    private static List<Integer> range(int from, int limit) {
        return IntStream.range(from, from + limit)
                .boxed()
                .collect(toList());
    }

    private static Try<Connection> createConnection() {
        try {
            return Try.success(DriverManager.getConnection(jdbcUrl, user, password));
        } catch (SQLException e) {
            return Try.failure(e);
        }
    }

    private static void trySetConnectionAutoCommit(Connection conn, boolean autoCommit) {
        try {
            conn.setAutoCommit(autoCommit);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void tryCommit(Connection conn) {
        try {
            conn.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
