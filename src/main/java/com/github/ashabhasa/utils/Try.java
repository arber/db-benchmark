package com.github.ashabhasa.utils;

import java.util.function.Supplier;

public class Try<T> {

    private final boolean isSuccess;
    private final T success;
    private final Throwable failure;

    private Try(T success, Throwable failure, boolean isSuccess) {
        this.success = success;
        this.failure = failure;
        this.isSuccess = isSuccess;
    }

    public static <T> Try<T> success(T success) {
        return new Try<T>(success, null, true);
    }

    public static <T> Try<T> failure(Throwable failure) {
        return new Try<T>(null, failure, false);
    }

    public static <T> Try<T> fromSupplier(String description, Supplier<T> supplier) {
        try {
            return success(supplier.get());
        } catch (Exception e) {
            return failure(new RuntimeException("Error converting to model " + description + ": " + e.getMessage(), e));
        }
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public boolean isFailure() {
        return !isSuccess;
    }

    public T get() {
        if (isSuccess())
            return success;
        else
            throw new RuntimeException(failure);
    }

    public Throwable getFailure() {
        return failure;
    }

}
