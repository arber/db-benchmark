package com.github.ashabhasa.models;

public class City {
    private final String name;
    private final String countryCode;
    private final String district;
    private final int population;

    public City(String name, String countryCode, String district, int population) {

        this.name = name;
        this.countryCode = countryCode;
        this.district = district;
        this.population = population;
    }

    public String getName() {
        return name;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public String getDistrict() {
        return district;
    }

    public int getPopulation() {
        return population;
    }
}
